Country Select for Drupal 6.x

REQUIRED MODULES:  addresses_cck and activeselect

INSTRUCTIONS 

Copy country_select.module and country_select.info into the addresses module directory
Install above two modules.  

Go to admin/settings/country_select to select the default country.  Country select recognises the addresses allowed country list.
Make sure that the country and province fields are turned off for the content type.  Country select creates its own form items and writes them to the same database field as the addresses module.  If both country and province fields are NOT turned off for the content type then country select does not work.

ENSURE THAT THE WEIGHT OF COUNTRY_SELECT MODULE IS GREATER THAN THE WEIGHT OF THE CONTENT MODULE.  The install function does this for you.